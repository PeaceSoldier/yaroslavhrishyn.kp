﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace kursa
{
    public partial class Form1 : Form
    {
        Bitmap pic;
        int x1, y1, xf1, yf1, xWidth = 60, yHeight = 60;
        string mode;
        public Form1()
        {
            InitializeComponent();
            Help();
            mode = "line";
            pic = new Bitmap(1000,1000);
            SolidBrush b = new SolidBrush(Color.White);
            Graphics.FromImage(pic).FillRectangle(b, 0, 0, pic.Width, pic.Height);
            pictureBox1.Image = pic;
            x1 = 0;
            y1 = 0;
        }
      
        void Form1_Load(object sender, EventArgs e)
        {

        }
        void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        void Help()
        {
            toolTip.SetToolTip(buttonLine, "Олівець");
            toolTip.SetToolTip(buttonDeleteAll, "Стерти все");
            toolTip.SetToolTip(buttonDelete, "Ластик");
            toolTip.SetToolTip(buttonSquare, "Квадрат");
            toolTip.SetToolTip(buttonСircle, "Коло");
            toolTip.SetToolTip(buttonOval, "Овал");
            toolTip.SetToolTip(buttonStar, "Зірка");
            toolTip.SetToolTip(buttonBackground, "Замалювати фон");
            toolTip.SetToolTip(buttonFontDialog, "Шрифт, колір та розмір тексту");
            toolTip.SetToolTip(buttonColorDialog, "Вибрати колір");
            toolTip.SetToolTip(trackBarVal, "Розмір олівця");
            toolTip.SetToolTip(buttonText, "Підтвердити свій вибір");
            toolTip.SetToolTip(buttonMain, "Колір, який зараз використовується");
            toolTip.SetToolTip(buttonValue, "Підтвердити розмір фігури");
            toolTip.SetToolTip(textBoxY, "Довжина");
            toolTip.SetToolTip(textBoxX, "Ширина");
            toolTip.SetToolTip(trackBar1, "Розмір ластику");
        }
        void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog.ShowDialog();
                if (openFileDialog.FileName != "")
                {
                    pic = (Bitmap)Image.FromFile(openFileDialog.FileName);
                    pictureBox1.Image = pic;
                }                
            }
            catch (Exception)
            {

            }            
        }
        void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            dr = MessageBox.Show("Зберегти зображення перед виходом?", "Вихід", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    {
                        saveToolStripMenuItem_Click(sender, e);
                        break;
                    }
                case DialogResult.No:
                    {
                        Application.Exit();
                        break;
                    }
            }

            
            Application.Exit();
        }
        void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog.ShowDialog();
                if (saveFileDialog.FileName != "")
                {
                    pic.Save(saveFileDialog.FileName);
                }
            }
            catch (Exception)
            {

            }
        }

       
        void buttonLine_Click(object sender, EventArgs e)
        {
            mode = "line";
        }
        void buttonSquare_Click(object sender, EventArgs e)
        {
            mode = "square";
        }
        void buttonСircle_Click(object sender, EventArgs e)
        {
            mode = "circle";
        }
        void buttonText_Click(object sender, EventArgs e)
        {
            mode = "text";
        }
        void buttonStar_Click(object sender, EventArgs e)
        {
            mode = "star";
        }
        void buttonMain_Click(object sender, EventArgs e)
        {
            Button b;
            b = (Button)sender;
            buttonMain.BackColor = b.BackColor;
        }
        void buttonDeleteAll_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            dr = MessageBox.Show("Ви точно хочете стрерти все?", "Стерти все", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    {
                        Graphics.FromImage(pic).Clear(Color.White);
                        pictureBox1.Image = pic;
                        break;
                    }
                case DialogResult.No:
                    {
                        break;
                    }

            }
        }
        void buttonDelete_Click(object sender, EventArgs e)
        {
            mode = "delete";
        }
        void buttonValue_Click(object sender, EventArgs e)
        {
            try
            {
                int X = Convert.ToInt32(textBoxX.Text);
                int Y = Convert.ToInt32(textBoxY.Text);
                xWidth = X;
                yHeight = Y;
            }
            catch (Exception)
            {

            }
        }
        void buttonOval_Click(object sender, EventArgs e)
        {
            mode = "oval";
        }
        void buttonBackground_Click(object sender, EventArgs e)
        {
            mode = "line";
            SolidBrush s = new SolidBrush(buttonMain.BackColor);
            Graphics.FromImage(pic).FillRectangle(s, 0, 0, pic.Width, pic.Height);
        }
        void buttonFontDialog_Click(object sender, EventArgs e)
        {
            fontDialog.ShowColor = true;
            fontDialog.Font = richTextBox.Font;
            fontDialog.Color = richTextBox.ForeColor;
            if (fontDialog.ShowDialog() != DialogResult.Cancel)
            {
                richTextBox.Font = fontDialog.Font;
                richTextBox.ForeColor = fontDialog.Color;
            }
        }
        void buttonColorDialog_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                buttonMain.BackColor = colorDialog.Color;
            }
        }
        
        
        void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            xf1 = e.X;
            yf1 = e.Y;
        }
        void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            Pen p = new Pen(buttonMain.BackColor, trackBarVal.Value);
            p.EndCap = LineCap.Round;
            p.StartCap = LineCap.Round;
            Graphics g = Graphics.FromImage(pic);
            if (e.Button == MouseButtons.Left)
            {
                if (mode == "line")
                {
                    g.DrawLine(p, x1, y1, e.X, e.Y);
                }
                if(mode == "delete")
                {
                    p = new Pen(Color.White, trackBar1.Value);
                    p.EndCap = LineCap.Round;
                    p.StartCap = LineCap.Round;
                    g.DrawLine(p, x1, y1, e.X, e.Y);
                }
                if (mode == "square")
                {
                    g.DrawRectangle(p, xf1 - 25, yf1 - 25, xWidth, yHeight);
                }
                if (mode == "circle")
                {
                    g.DrawEllipse(p, xf1 - 25, yf1 - 25, xWidth , xWidth);
                }
                if (mode == "oval")
                {
                    g.DrawEllipse(p, xf1 - 25, yf1 - 25, xWidth + 40, yHeight);
                }
                if (mode == "text")
                {
                    SolidBrush TextColor = new SolidBrush(richTextBox.ForeColor);
                    string buf = richTextBox.Text;
                    g.DrawString(buf, richTextBox.Font, TextColor, new PointF(xf1 -25, yf1 -25));
                }
                if (mode == "star")
                {
                    int n = 5;
                    double R = 25, r = 50;
                    double alpha = 0;
                    double x0 = xf1, y0 = yf1;
                    PointF[] points = new PointF[2 * n + 1];
                    double a = alpha, da = Math.PI / n, l;
                    for (int k = 0; k < 2 * n + 1; k++)
                    {
                        l = k % 2 == 0 ? r : R;
                        points[k] = new PointF((float)(x0 + l * Math.Cos(a)), (float)(y0 + l * Math.Sin(a)));
                        a += da;
                    }
                    g.DrawLines(p, points);
                }
                pictureBox1.Image = pic;
            }
           x1 = e.X;
           y1 = e.Y;
        }
    }
}
